public class Application
{
 public static void main(String[] args)
 {
  Cheetah cheetah = new Cheetah();
  
  cheetah.weight = 70.0;
  cheetah.speed = 65.0;
  cheetah.diet = "Carnivore";
  
  Cheetah cheetah2 = new Cheetah();
  
  cheetah2.weight = 40.0;
  cheetah2.speed = 45.0;
  cheetah2.diet = "Carnivore";
  
  cheetah.topSpeed();
  cheetah2.topSpeed();
  
  Cheetah[] coalition = new Cheetah[3];
  
  coalition[0] = cheetah;
  coalition[1] = cheetah2;
  
  System.out.println(coalition[0].weight);
  
  coalition[3] = new Cheetah();
  
  cheetah3.weight = 39.0;
  cheetah3.speed = 67.0;
  cheetah3.diet = "Carnivore";
 }
}